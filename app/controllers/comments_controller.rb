class CommentsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_article

  def create
    @comment = @article.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      ActionCable.server.broadcast "comments", render(partial: 'comments/comment', object: @comment)
      flash.now[:notice] = "Comment has been created"
    else
      flash.now[:alert] = "Comment has not been created"
      render current_path
    end

  end

  private

    def comment_params
      params.require(:comment).permit(:body)
    end

    def set_article
      @article = Article.find_by(id: params[:article_id])
    end

end
