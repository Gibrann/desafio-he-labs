class ArticlesController < ApplicationController

  before_action :authenticate_user!, except: [:show, :index]
  before_action :set_article, only: [:show, :destroy, :update, :edit]

  def index
    @articles = Article.all.order(created_at: "desc")
  end

  def my_articles
    @articles = Article.where("user_id = ?", current_user.id).order(created_at: "desc")
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.user = current_user
    if @article.save
      flash[:notice] = "Article has been created"
      redirect_to articles_path
    else
      flash[:danger] = "Article has not been created"
      render :new
    end
  end

  def show
    if @article.nil?
      flash[:alert] = "The article you are looking for could not be found"
      redirect_to articles_path
    else
      @comment = @article.comments.build
      @comments = @article.comments
    end
  end

  def edit
    unless @article.user.id == current_user.id
      flash.now[:alert] = "You can only edit your own articles"
      redirect_to root_path
    end
  end

  def update
    unless @article.user.id == current_user.id
      flahs.now[:alert] = "You can only edit your own articles"
      redirect_to root_path
    else
      if @article.update(article_params)
        flash[:notice] = "Article has been updated"
        redirect_to root_path
      else
        flash.now[:danger] = "Article has not been updated"
        render :edit
      end
    end
  end

  def destroy
    if @article.user.id == current_user.id
      if @article.destroy
        flash[:notice] = "The Article has been deleted"
      else
        flash[:alert] = "The Article could not be deleted"
      end
    else
      flash[:alert] = "You can only delete your own articles"
    end
    redirect_to root_path
  end

  private

    def set_article
      @article = Article.find_by(id: params[:id])
    end

    def article_params
      params.require(:article).permit(:title, :body)
    end

end
