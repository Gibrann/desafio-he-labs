class NotificationsController < ApplicationController

    skip_before_action :verify_authenticity_token    

    def mark_as_read
        notifications = Notification.where(user_id: params[:id])
        notifications.each do |notification| 
            notification.update(read: true)
        end
    end

end
