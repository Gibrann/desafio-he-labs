class FollowersController < ApplicationController

    before_action :authenticate_user!
    before_action :set_user, only: [ :create ]

    def create
        unless is_already_following
            @follower = Follower.new(user_id: params[:user_id], followed_by: current_user.id)
            if @follower.save
                @following = true;
                user = User.find(params[:user_id])
                notification = user.notifications.build
                notification.description = "You started to be followed by #{current_user.first_name}"
                notification.save
            else
                return false
            end
        else
            @following = false
            @follower = Follower.find_by("user_id = ? AND followed_by = ? ", params[:user_id], current_user.id)
            @follower.destroy
        end
    end

    private

        def is_already_following
            current_user.is_following(@user)
        end

        def set_user
           @user = User.find(params[:user_id])
        end

end