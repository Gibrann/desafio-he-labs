class UsersController < ApplicationController
    
    before_action :authenticate_user!, only: [ :index ]
    before_action :set_user, only: [ :profile ]

    def index
        render json: { data: query_response }, status: :ok
    end

    def profile
        unless @user
            flash.now[:alert] = "User not found"
        end
    end

    private

        def query_response
            users = User.find_by_query(params[:query])
            data = []
            users.each do |user|
                data << {
                    full_name: user.full_name,
                    url_profile: "/user/#{user.id}/profile"
                } 
            end if users.any?
            data
        end

        def set_user
            @user = User.find(params[:id])
        end


end

