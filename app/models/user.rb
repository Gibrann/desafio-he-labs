class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :comments
  has_many :articles
  has_many :notifications

  validates :email, uniqueness: true, allow_blank: true

  validates :first_name, :last_name, :email, presence: true

  scope :not_current_user, ->(id) { where.not("id = ?", id) }

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.find_by_query(query)
    User.where("first_name LIKE ? OR last_name LIKE ?", "%#{query}%", "%#{query}%")
  end

  def followers
    Follower.where("user_id = ?", self.id)
  end

  def avatar
    self.gender == "M" ? "https://image.ibb.co/gTG79k/male.png" : "https://image.ibb.co/gVyyFQ/female.jpg"
  end

  def is_following(user)
    Follower.where("user_id = ? AND followed_by = ?", user.id, self.id).count == 0 ? false : true
  end

end
