class Notification < ApplicationRecord
  
  belongs_to :user

  default_scope { where(read: false) }

end
