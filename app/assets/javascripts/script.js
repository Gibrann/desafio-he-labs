(function(){

	function loadScript() {

		"use strict";

		$(".ui.dropdown").dropdown();

		$('.message .close').on('click', function() {
			$(this).closest('.message')
				.transition('fade');
		});

		//search users
		$('.ui.search')
			.search({
				apiSettings: {
					url: '/users?query={query}',
					onResponse: function(result) {
						const users = result.data;
						users.map(function(user) { 
							user.full_name = [user.first_name, user.last_name].join(" "); 
							return user; 
						});
					}
				  },
				  fields: {
					results : 'data',
					title   : 'full_name',
					url     : 'url_profile'
				  },
				  minCharacters : 3
			});

		//mark notifications as read
		var sup = document.querySelector("sup");
		var dropdown = document.querySelector("div.ui.dropdown.item");
		var user_id = document.querySelector("input[type='hidden']");

		
		if (!dropdown) return;

		dropdown.addEventListener("click", function(){

			if (sup.textContent === "0") return;

			if (dropdown.classList.contains("visible")) return;

			$.ajax({
				method: "PUT",
				url: "/notifications/".concat(user_id.dataset.user).concat("/user")
			  }).done(function( msg ) {
					sup.textContent = "0";
			});

		});

	}

	document.addEventListener("turbolinks:load", function() {
	   loadScript();
	});

})();