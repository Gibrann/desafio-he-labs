Rails.application.routes.draw do

  mount ActionCable.server => "/cable"

  root to: 'articles#index'

  resources :articles do
    resources :comments
  end

  get 'my_articles', to: 'articles#my_articles'
  get 'users', to: 'users#index'
  get 'user/:id/profile', to: 'users#profile', as: 'profile'
  put 'notifications/:id/user', to: 'notifications#mark_as_read'

  post 'follow/user', to: 'followers#create', as: "follow_user"
  delete 'follow/user', to: 'followers#destroy', as: "follow_userr"

  devise_for :users

end
