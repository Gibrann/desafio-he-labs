require 'rails_helper'

RSpec.feature "Creating Articles" do

  before do
    @john = User.create(email: "john@example.com", password: "password", first_name: "John", last_name: "Silva")
    @fred = User.create(email: "fred@example.com", password: "password", first_name: "Fred", last_name: "Silva")
    @article = Article.create!(title: "Title one", body: "Body of the article", user: @fred)
  end

  scenario "permits a signed in user to write a review" do
    login_as(@fred)
    visit "/"
    click_link @article.title
    fill_in "New Comment", with: "An awesome article"
    click_button "Create Comment"

    expect(page).to have_content("An awesome article")

  end

end