require 'rails_helper'

RSpec.describe "Comments" do

  before do
    @john = User.create(email: "john@example.com", password: "password", first_name: "John", last_name: "Silva")
    @fred = User.create(email: "fred@example.com", password: "password", first_name: "Fred", last_name: "Silva")
    @article = Article.create!(title: "Title one", body: "Body of the article", user: @fred)
  end

  describe 'POST /articles/:id/comments' do
    context 'with no signed-in user' do
      before do
        post "/articles/#{@article.id}/comments", params: { comment: { body: "My First Comment" } }
      end

      it "redirect user to sign in page" do
        flash_message = "You need to sign in or sign up before continuing."
        expect(response).to redirect_to(new_user_session_path)
        expect(response.status).to eq 302
        expect(flash[:alert]).to eq flash_message
      end
    end

    context 'with signed-in user' do
      before do
        login_as(@john)
        post "/articles/#{@article.id}/comments", params: { comment: { body: "My First Comment" } }
      end

      it "create comment successfully" do
        expect(response.status).to eq 200
      end
    end

  end

end
